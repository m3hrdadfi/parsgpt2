#!/usr/bin/env python3
# Usage:
#  PYTHONPATH=src ./encode.py <file|directory|glob> /path/to/output.npz
#  PYTHONPATH=src ./train --dataset /path/to/output.npz

import argparse
import numpy as np

import encoder
from load_dataset import load_dataset

parser = argparse.ArgumentParser(
    description='Pre-encode text files into tokenized training set.',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--model_path', type=str, default='models', help='Pretrained model path')
parser.add_argument('--model_name', metavar='MODEL', type=str, default='117M', help='Pretrained model name')
parser.add_argument('--combine', metavar='CHARS', type=int, default=50000,
                    help='Concatenate files with <|endoftext|> separator into chunks of this minimum size')
parser.add_argument('--encoding', type=str, default='utf-8', help='Set the encoding for reading and writing files.')
parser.add_argument('--remove_eos_token', default=False, action='store_true', help='Remove <|endoftext|> token')

parser.add_argument('--pad_token', type=str, default='<|pad|>', help='HighSpeedTokenizer pad_token')
parser.add_argument('--padding_side', type=str, default='right', help='HighSpeedTokenizer padding_side')
parser.add_argument('--pad_token_id', type=int, default=0, help='HighSpeedTokenizer pad_token_id')
parser.add_argument('--pad_token_type_id', type=int, default=0, help='HighSpeedTokenizer pad_token_type_id')
parser.add_argument('--max_length', type=int, default=1024, help='HighSpeedTokenizer max_length')
parser.add_argument('--padding', default=False, action='store_true', help='HighSpeedTokenizer padding')
parser.add_argument('--pad_to_max_length', default=False, action='store_true',
                    help='HighSpeedTokenizer pad_to_max_length')
parser.add_argument('--truncate', default=False, action='store_true', help='HighSpeedTokenizer truncate')

parser.add_argument('in_text', metavar='PATH', type=str, help='Input file, directory, or glob pattern (utf-8 text).')
parser.add_argument('out_npz', metavar='OUT.npz', type=str, help='Output file path')


def main():
    args = parser.parse_args()
    enc = encoder.get_encoder(
        model_name=args.model_name,
        model_path=args.model_path,
        pad_token=args.pad_token,
        padding=args.padding,
        pad_to_max_length=args.pad_to_max_length,
        padding_side=args.padding_side,
        pad_token_id=args.pad_token_id,
        pad_token_type_id=args.pad_token_type_id,
        truncate=args.truncate,
        max_length=args.max_length)

    print('Reading files')
    chunks = load_dataset(
        enc,
        args.in_text,
        args.combine,
        encoding=args.encoding,
        remove_eos_token=args.remove_eos_token)

    print('Writing', args.out_npz)
    np.savez_compressed(args.out_npz, *chunks)


if __name__ == '__main__':
    main()
